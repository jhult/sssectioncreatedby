package com.jonathanhult.webcenter.content.sssectioncreatedby;

import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.COMPONENT_ENABLED;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.D_NAME;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.IDC_SERVICE;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.NEW_NODE_ID;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.NODE_ID;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.PROPERTY;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.PROPERTY_NAME;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.SITE_ID;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.SS_SET_NODE_PROPERTY;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.TRACE_SECTION;
import static com.jonathanhult.webcenter.content.sssectioncreatedby.SSSectionCreatedByConstants.VALUE;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import static intradoc.shared.SharedObjects.getEnvironmentValue;
import intradoc.common.ExecutionContext;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;
import intradoc.shared.UserData;

/**
 * @author Jonathan Hult
 * {@link http://jonathanhult.com}
 */
public class SiteStudioAddNodeFilter implements FilterImplementor {

	private static UserData m_userData;
	private static String m_userName;

	/**
	 * This method implements a SiteStudioAddNode filter for the SSSectionCreatedBy component.
	 * First, it checks to determine whether the component is enabled by the preference prompt SSSectionCreatedBy_ComponentEnabled.
	 * Second, it finds the current user who is adding a section.
	 * Third, it calls the service SS_SET_NODE_PROPERTY.
	 * This service sets the user name as the value for the attribute name defined in the preference prompt SSSectionCreatedBy_PropertyName.
	 */
	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		trace("Start doFilter for SiteStudioAddNode for SSSectionCreatedBy");

		try {
			// Exit if component is not enabled
			if (!getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				trace("Component not enabled");
				return CONTINUE;
			}

			// DataBinder to execute service SS_SET_NODE_PROPERTY
			final DataBinder serviceBinder = new DataBinder();

			// Get siteId
			final String siteId = binder.getLocal(SITE_ID);

			// Get nodeId
			final String nodeId = binder.getLocal(NEW_NODE_ID);

			// Put necessary service parameters into serviceBinder
			serviceBinder.putLocal(SITE_ID, siteId);
			serviceBinder.putLocal(NODE_ID, nodeId);
			serviceBinder.putLocal(PROPERTY, getEnvironmentValue(PROPERTY_NAME));
			serviceBinder.putLocal(VALUE, getUserName((Service)ctx));

			// Execute SS_SET_NODE_PROPERTY service
			executeService(serviceBinder, (Service)ctx, SS_SET_NODE_PROPERTY);

			return CONTINUE;
		} finally {
			trace("End doFilter for SiteStudioAddNode for SSSectionCreatedBy");
		}
	}

	/**
	 * This method executes a service.
	 * @param serviceBinder The DataBinder which contains information about the service call.
	 * @param service The Service from the original service call.
	 * @param serviceName The name of the Service from to execute.
	 * @throws ServiceException
	 */
	public static void executeService(final DataBinder serviceBinder, final Service service, final String serviceName) throws ServiceException {
		traceVerbose("Start executeService");

		// Put service name to execute in serviceBinder
		serviceBinder.putLocal(IDC_SERVICE, serviceName);
		try {
			trace("Calling service " + serviceName + ": " + serviceBinder.getLocalData().toString());
			// Execute service
			service.getRequestImplementor().executeServiceTopLevelSimple(serviceBinder, serviceName, getUserData(service));
			trace("Finished calling service");
		} catch (final DataException e) {
			trace("Something went wrong executing service " + serviceName);
			e.printStackTrace(System.out);
			throw new ServiceException("Something went wrong executing service " + serviceName, e);
		} finally {
			traceVerbose("End executeService");
		}
	}

	/**
	 * This method retrieves the UserData from the ExecutionContext/Service.
	 * @param The Service from ExecutionContext.
	 * @return UserData for current service user.
	 */
	public static UserData getUserData(final Service service) {
		traceVerbose("Start getUserData");

		try {
			if (m_userData != null) {
				return m_userData;
			}
			// Retrieve UserData
			m_userData = service.getUserData();

			// Retrieve User name
			m_userName = m_userData.getProperty(D_NAME);
			trace("Username: " + m_userName);

			return m_userData;
		} finally {
			traceVerbose("End getUserData");
		}
	}

	/**
	 * This method retrieves the current user name from the ExecutionContext/Service.
	 * @param The Service from ExecutionContext.
	 * @return User name for current service user.
	 */
	public static String getUserName(final Service service) {
		traceVerbose("Start getUserName");

		try {
			if (m_userName != null) {
				return m_userName;
			}

			// Get UserData
			getUserData(service);

			// User name will now be available
			return m_userName;

		} finally {
			traceVerbose("End getUserName");
		}
	}

	/**
	 * This methods traces output to the WebCenter Content console.
	 * @param message The String to trace to the WebCenter Content console.
	 */
	public static void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	/**
	 * This method verbose traces output to the WebCenter Content console.
	 * @param message The String to trace to the WebCenter Content console.
	 */
	public static void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}
}

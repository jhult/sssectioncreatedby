package com.jonathanhult.webcenter.content.sssectioncreatedby;

/**
 * @author Jonathan Hult
 * {@link http://jonathanhult.com}
 */
public class SSSectionCreatedByConstants {
	public static final String IDC_SERVICE = "IdcService";
	public static final String SS_SET_NODE_PROPERTY = "SS_SET_NODE_PROPERTY";
	public static final String SITE_ID = "siteId";
	public static final String NODE_ID = "nodeId";
	public static final String NEW_NODE_ID = "newNodeId";
	public static final String PROPERTY = "property";
	public static final String PROPERTY_NAME = "SSSectionCreatedBy_PropertyName";
	public static final String VALUE = "value";
	public static final String D_NAME = "dName";
	public static final String TRACE_SECTION = "sssectioncreatedby";
	public static final String COMPONENT_ENABLED = "SSSectionCreatedBy_ComponentEnabled";
}

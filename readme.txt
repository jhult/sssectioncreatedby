﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              SSSectionCreatedBy
	Author:                 Jonathan Hult
							http://jonathanhult.com
	Last Updated On:        build_1_20121025

*******************************************************************************
** Overview
*******************************************************************************

	This component implements a SiteStudioAddNode filter. It adds the user 
	name of the user who creates a Site Studio section as an attribute on the 
	section in the Site Studio project file. It does this by calling the 
	service SS_SET_NODE_PROPERTY.
	
	Filters:
	SiteStudioAddNode - This filter gets called during the creation of a new 
	Site Studio node (service = SS_ADD_NODE).
		
	Preference Prompts:
	SSSectionCreatedBy_ComponentEnabled - Boolean determining if the 
	component functionality is enabled.
	SSSectionCreatedBy_PropertyName - The name of the section property 
	containing the user who created the section.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

build_1_20121025
	- Initial component release